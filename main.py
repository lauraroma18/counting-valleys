import counting_valleys

ar = ['U','D','D','D','U','D','U','U']

def main():
    print(counting_valleys.countingValleys(8,ar))

if __name__ == '__main__':
    main()
