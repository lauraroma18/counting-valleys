import unittest
import counting_valleys

class TestCountingValleys(unittest.TestCase):
    
    def test_counting_valleys(self):
        self.assertAlmostEqual(
            counting_valleys.countingValleys(8, ['U','D','D','D','U','D','U','U']), 1)
        self.assertAlmostEqual(
            counting_valleys.countingValleys(12, ['D','D','U','U','D','D','U','D','U','U','U','D']), 2)

if __name__ == '__main__':
    unittest.main(argv=[''],verbosity=2, exit=False)
