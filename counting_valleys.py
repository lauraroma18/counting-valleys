#!/bin/python3

import math
import os
import random
import re
import sys

#
# Verify if he came back to sea level from a valley
# The function accepts following parameters:
#  1. INTEGER before st
#  2. INTEGER level where he is now, zero equal to sea level 
#
def isValley(before_step, sea_level):
    if(sea_level == 0 and before_step == 'U'):
        return 1
    else:
        return 0
    
#
# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path
#    
def countingValleys(steps, path):
    sea_level = 0
    valleys = 0
    
    for step in path:
        if(step == 'U'):
            sea_level +=1            
            valleys += isValley(step, sea_level)
            
            continue
        
        if(step == 'D'):
            sea_level -=1
    
    return valleys
